let name = prompt("What's your name?");
let tryAgain = "Please, try again!";
let notAllowed = "You are not allowed to visit this website";
let wantToContinue = "Are you sure you want to continue?";
while (name == undefined || !isNaN(name)) {
	if (name == undefined) {
		name = prompt(tryAgain, "");
	} else {
		name = prompt(tryAgain, name);
	}
}
let age = prompt("How old are you?");
while (age <= 0 || isNaN(age)) {
	if (age == null) {
		age = prompt(tryAgain, "");
	} else {
		age = prompt(tryAgain, age);
	}
}
if (age < 18) {
	alert(notAllowed);
} else if (age >= 18 && age <= 22) {
	let answer = confirm(wantToContinue);
	if (answer) {
		alert(`Welcome, ${name}`);
	} else {
		alert(notAllowed);
	}
} else if (age > 22) {
	alert(`Welcome, ${name}`);
}
